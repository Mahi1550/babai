//
//  ContactsRepository.swift
//  Contacts
//
//  Created by Mahender Reddy Gaddam on 1/8/20.
//  Copyright © 2020 Mahender Reddy Gaddam. All rights reserved.
//

import Foundation

protocol ContactsRepositoryProtocol {
    var contacts: [Contact]? { get set }

    func loadContacts(handler: @escaping (Bool) -> Void)
    func loadSelectedContact(id: Int, handler: @escaping (Bool) -> Void)
    func searchContacts(with: String) -> [Contact]
}

class ContactsRepository: ContactsRepositoryProtocol {
    var contacts: [Contact]?
    let contactService: ContactService
    
    init(service: ContactService) {
        contactService = service
    }

    func loadContacts(handler: @escaping (Bool) -> Void) {
        contactService.request(serviceType: .getContacts, model: [Contact].self) { [weak self] (newContacts, response, error) in
            if error == nil {
                self?.contacts = newContacts
                handler(true)
            } else {
                handler(false)
            }
        }
    }
    
    func loadSelectedContact(id: Int, handler: @escaping (Bool) -> Void) {
        contactService.request(serviceType: .getContact(id: id), model: Contact.self) { [weak self] (data, response, error) in
            if error == nil {
                let index = self?.contacts?.firstIndex { $0.id == id }
                if index != nil && data != nil {
                    self?.contacts?[index!] = data!
                }
                handler(true)
            } else {
                handler(false)
            }
        }
    }
    
    func searchContacts(with pattern: String) -> [Contact] {
        return self.contacts?.filter({ contact in
            let lastName = contact.lastName?.lowercased()
            return lastName?.contains(pattern.lowercased()) ?? false
        }) ?? []
    }
}
