//
//  ContactsRepositoryTests.swift
//  ContactsTests
//
//  Created by Mahender Reddy Gaddam on 1/8/20.
//  Copyright © 2020 Mahender Reddy Gaddam. All rights reserved.
//

import XCTest
@testable import Contacts

class ContactsRepositoryTests: XCTestCase {
    var repository: ContactsRepositoryProtocol!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testLoadContactsReturnsAllContacts() {
        let networkStub = NetworkClientStub()
        let contactService = ContactService(networkClient: networkStub)
        repository = ContactsRepository(service: contactService)
        
        let expectation = self.expectation(description: "Should return 2 contacts")
        repository.loadContacts { success in
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 3, handler: nil)
        
        let actual = self.repository.contacts?.count
        let expected = 2
        
        XCTAssertEqual(actual, expected)
    }
    
    func testGetContactReturnsCorrectContact() {
        let networkStub = NetworkClientStub()
        let contactService = ContactService(networkClient: networkStub)
        repository = ContactsRepository(service: contactService)
        repository.contacts = [Contact(id: 123)]
        
        let expectation = self.expectation(description: "Should return 1 contact")
        repository.loadSelectedContact(id: 123, handler: { success in
            if success {
                expectation.fulfill()
            }
        })
        
        waitForExpectations(timeout: 3, handler: nil)
        
        let actual = self.repository.contacts?.count
        let expected = 1
        
        XCTAssertEqual(actual, expected)
    }
    
    func testSearchContactsReturnsMatchingContacts() {
        let networkStub = NetworkClientStub()
        let contactService = ContactService(networkClient: networkStub)
        repository = ContactsRepository(service: contactService)
        repository.contacts = [ContactBuilder().withFirstName(firstName: "Mahender").withLastName(lastName: "Gaddam").build(),
        ContactBuilder().withFirstName(firstName: "Supriyo").withLastName(lastName: "Mondal").build()]
        
        let actual = repository.searchContacts(with: "M").count
        let expected = 2
        
        XCTAssertEqual(actual, expected)
    }
}
